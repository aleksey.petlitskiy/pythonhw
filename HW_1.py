# 1) Создать переменную типа String

name = 'Oleksii'

# 2) Создать переменную типа Integer

age = 33

# 3) Создать переменную типа Float

weight = 83.3

# 4) Создать переменную типа Bytes

value = b'10'

# 5) Создать переменную типа List

friends = ['Tim', 'Roman', 'Andrii']

# 6) Создать переменную типа Tuple

family = ('Alla', 'Yurii', 'Helen', 'Julia', 4)

# 7) Создать переменную типа Set

numbers = {1, 2, 3, 4, 5}

# 8. Создать переменную типа Frozen set

numbers2 = frozenset(numbers)

# 9) Создать переменную типа Dict

couple = {
    'boy': "Oleksii",
    'girl': 'Julia'
}

# 10) Вывести в консоль все выше перечисленные переменные с добавлением типа данных.
print(
    name + ' type is ', type(name), "\n",
    str(age) + ' type is ', type(age), "\n",
    str(weight) + ' type is ', type(weight), "\n",
    str(value) + ' type is ', type(value), "\n",
    str(friends) + ' type is ', type(friends), "\n",
    str(family) + ' type is ', type(family), "\n",
    str(numbers) + ' type is ', type(numbers), "\n",
    str(numbers2) + ' type is ', type(numbers2), "\n",
    str(couple) + ' type is ', type(couple), "\n",
)
# 12) Вывести в одну строку переменные типа String и Integer используя “,” (Запятую)
# 13) Вывести в одну строку переменные типа String и Integer используя “+” (Плюс)
